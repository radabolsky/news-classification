import torch


def export_to_onnx(model, input, save_path):
    onnx_program = torch.onnx.dynamo_export(model, input)
    onnx_program.save(save_path)