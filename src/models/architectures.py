import torch
from torch import nn
import torch.nn.functional as F
from torch.autograd import Variable

class SimpleRNN(nn.Module):
    def __init__(self, hidden_dim, vocab_size, num_classes, dropout_p=0.1):
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, hidden_dim)
        self.rnn = nn.RNN(hidden_dim, hidden_dim)
        self.linear = nn.Linear(hidden_dim, hidden_dim)
        self.projection = nn.Linear(hidden_dim, num_classes)

        self.activation = nn.Tanh()
        self.dropout = nn.Dropout(p=dropout_p)

    def forward(self, input_batch):
        embs = self.embedding(input_batch)
        out, _ = self.rnn(embs)

        out = out.mean(dim=1)
        out = self.dropout(self.linear(self.activation(out)))
        pred = self.projection(self.activation(out))
        return pred
    

class SimpleLSTM(nn.Module):
    def __init__(self, embedding_dim, hidden_dim, vocab_size, num_classes, n_layers=1, dropout_p=0.1, bidirectional=False):
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, 
                            hidden_dim,
                            num_layers=n_layers,
                            bidirectional=bidirectional,
                            dropout=dropout_p,
                            batch_first=True)
        self.n_layers = n_layers
        if bidirectional:
            self.linear = nn.Linear(hidden_dim * 2, num_classes)
        else:
            self.linear = nn.Linear(hidden_dim, num_classes)
        self.dropout = nn.Dropout(dropout_p)
        self.activation = nn.Sigmoid()

    def forward(self, input_batch):
        embs = self.embedding(input_batch)
        batch_size = input_batch.size()[0]
        h0 = Variable(torch.zeros(self.n_layers, batch_size, self.lstm.hidden_size).to(self.device))
        c0 = Variable(torch.zeros(self.n_layers, batch_size, self.lstm.hidden_size).to(self.device))

        packed_output, (hidden, cell) = self.lstm(embs, (h0, c0))
        output = self.linear(hidden[-1])
        output = self.activation(output)
        return output
    

class AttentionLSTM(nn.Module):
    def __init__(self, embedding_dim, hidden_dim, vocab_size, num_classes, n_layers=1, dropout_p=0.1, bidirectional=False):
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, 
                            hidden_dim,
                            num_layers=n_layers,
                            bidirectional=bidirectional,
                            dropout=dropout_p,
                            batch_first=True)
        if bidirectional:
            self.linear = nn.Linear(hidden_dim * 2, num_classes)
        else:
            self.linear = nn.Linear(hidden_dim, num_classes)
        self.dropout = nn.Dropout(dropout_p)
        self.activation = nn.Softmax(dim=1)

    def attention(self, lstm_out, final_state):
        hidden = final_state.squeeze(0)
        attn_weights = torch.bmm(lstm_out, hidden.unsqueeze(2)).squeeze(2)
        soft_attn_weights = nn.functional.softmax(attn_weights, dim=1)
        new_hidden_state = torch.bmm(lstm_out.transpose(1, 2), soft_attn_weights.unsqueeze(2)).squeeze(2)
        return new_hidden_state

    def forward(self, input_batch):
        embs = self.embedding(input_batch)
        batch_size = input_batch.size()[0]
        h0 = Variable(torch.zeros(1, batch_size, self.lstm.hidden_size).to(self.device))
        c0 = Variable(torch.zeros(1, batch_size, self.lstm.hidden_size).to(self.device))

        packed_output, (hidden, cell) = self.lstm(embs, (h0, c0))
        attn_output = self.attention(packed_output, hidden)
        output = self.activation(attn_output)
        return output
    

class SimpleGRU(nn.Module):
    def __init__(self, embedding_dim, hidden_dim, vocab_size, num_classes, n_layers=1, dropout_p=0.1, bidirectional=False):
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.gru = nn.GRU(embedding_dim, 
                            hidden_dim,
                            num_layers=n_layers,
                            bidirectional=bidirectional,
                            dropout=dropout_p,
                            batch_first=True)
        self.n_layers = n_layers
        if bidirectional:
            self.linear = nn.Linear(hidden_dim * 2, num_classes)
        else:
            self.linear = nn.Linear(hidden_dim, num_classes)
        self.dropout = nn.Dropout(dropout_p)
        self.activation = nn.LogSoftmax(dim=1)


    def forward(self, input_batch):
        embs = self.embedding(input_batch)
        batch_size = input_batch.size()[0]
        h0 = Variable(torch.zeros(self.n_layers, batch_size, self.gru.hidden_size).to(self.device))
        hidden, _ = self.gru(embs, h0)
        output = self.linear(hidden[:, -1, :])
        output = self.activation(output)
        return output


class EmbFCModel(nn.Module):
    def __init__(self,  embedding_dim, vocab_size, num_classes):
        super(EmbFCModel, self).__init__()
        self.embedding = nn.EmbeddingBag(vocab_size, embedding_dim, sparse=False)
        self.fc = nn.Linear(embedding_dim, num_classes)
        self.init_weights()

    def init_weights(self):
        initrange = 0.5
        self.embedding.weight.data.uniform_(-initrange, initrange)
        self.fc.weight.data.uniform_(-initrange, initrange)
        self.fc.bias.data.zero_()

    def forward(self, input_batch):
        embedded = self.embedding(input_batch)
        return self.fc(embedded)


class SoftmaxLSTM(nn.Module):
    def __init__(self, embedding_dim, hidden_dim, vocab_size, num_classes, n_layers=1, dropout_p=0.1, bidirectional=False):
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, 
                            hidden_dim,
                            num_layers=n_layers,
                            bidirectional=bidirectional,
                            dropout=dropout_p,
                            batch_first=True)
        if bidirectional:
            self.linear = nn.Linear(hidden_dim * 2, num_classes)
        else:
            self.linear = nn.Linear(hidden_dim, num_classes)
        self.dropout = nn.Dropout(dropout_p)
        self.activation = nn.Softmax()

    def forward(self, input_batch):
        embs = self.embedding(input_batch)
        batch_size = input_batch.size()[0]
        h0 = Variable(torch.zeros(1, batch_size, self.lstm.hidden_size).to(self.device))
        c0 = Variable(torch.zeros(1, batch_size, self.lstm.hidden_size).to(self.device))

        packed_output, (hidden, cell) = self.lstm(embs, (h0, c0))
        output = self.linear(hidden[-1])
        output = self.activation(output)
        return output
    
class TanhLSTM(nn.Module):
    def __init__(self, embedding_dim, hidden_dim, vocab_size, num_classes, n_layers=1, dropout_p=0.1, bidirectional=False):
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, 
                            hidden_dim,
                            num_layers=n_layers,
                            bidirectional=bidirectional,
                            dropout=dropout_p,
                            batch_first=True)
        if bidirectional:
            self.linear = nn.Linear(hidden_dim * 2, num_classes)
        else:
            self.linear = nn.Linear(hidden_dim, num_classes)
        self.dropout = nn.Dropout(dropout_p)
        self.activation = nn.Tanh()

    def forward(self, input_batch):
        embs = self.embedding(input_batch)
        batch_size = input_batch.size()[0]
        h0 = Variable(torch.zeros(1, batch_size, self.lstm.hidden_size).to(self.device))
        c0 = Variable(torch.zeros(1, batch_size, self.lstm.hidden_size).to(self.device))

        packed_output, (hidden, cell) = self.lstm(embs, (h0, c0))
        # hidden = self.dropout(torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim = 1))
        output = self.linear(self.activation(hidden[-1]))
        # output = self.dropout(self.fc2(output))
        # output = self.activation(output)
        return output

class FixedSimpleRNN(nn.Module):
    def __init__(self, hidden_dim, vocab_size, num_classes):
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, hidden_dim)
        self.rnn = nn.RNN(hidden_dim, hidden_dim)
        self.linear = nn.Linear(hidden_dim, hidden_dim)
        self.projection = nn.Linear(hidden_dim, num_classes)

        self.activation = nn.Tanh()
        self.dropout = nn.Dropout(p=0.1)

    def forward(self, input_batch):
        embs = self.embedding(input_batch)
        out, _ = self.rnn(embs)

        out = out.mean(dim=1)
        out = self.dropout(self.linear(self.activation(out)))
        pred = self.projection(self.activation(out))
        return pred


class AdaptiveDropout(nn.Module):
    def __init__(self):
        super(AdaptiveDropout, self).__init__()

    def forward(self, input, p):
        if self.training:
            p = 1. - p.data
            temp = torch.rand(input.size()).cuda() < p
            temp = torch.autograd.Variable(temp.type_as(p) / p)
            input = torch.mul(input, temp)
            return input
        else:
            return input


class LockedDropout(torch.nn.Module):
    def __init__(self, dropout=None):
        super(LockedDropout, self).__init__()
        self.dropout = dropout

    def forward(self, x):
        if not self.training or not self.dropout:
            return x
        m = x.data.new(1, x.size(1), x.size(2)).bernoulli_(1 - self.dropout)
        mask = torch.autograd.Variable(m, requires_grad=False) / (
                    1 - self.dropout)
        mask = mask.expand_as(x)
        return mask * x


def embedded_dropout(embed, words, dropout=0.1, scale=None):
    if dropout:
        mask = embed.weight.data.new().resize_(
            (embed.weight.size(0), 1)).bernoulli_(1 - dropout).expand_as(
            embed.weight) / (1 - dropout)
        mask = torch.autograd.Variable(mask)
        masked_embed_weight = mask * embed.weight
    else:
        masked_embed_weight = embed.weight
    if scale:
        masked_embed_weight = scale.expand_as(
            masked_embed_weight) * masked_embed_weight

    padding_idx = embed.padding_idx
    if padding_idx is None:
        padding_idx = -1

    X = F.embedding(words,
                    masked_embed_weight,
                    padding_idx,
                    embed.max_norm,
                    embed.norm_type,
                    embed.scale_grad_by_freq,
                    embed.sparse)
    return X


def embedded_adaptive_dropout(embed, words, dropout=0.1, scale=None,
                              is_training=True):
    if is_training:
        mask = embed.weight.data.new().resize_(
            (embed.weight.size(0), 1)).bernoulli_(1 - dropout)
        mask = (mask / torch.unsqueeze((1 - dropout), -1)).expand_as(
            embed.weight)
        mask = torch.autograd.Variable(mask)
        masked_embed_weight = mask * embed.weight
    else:
        masked_embed_weight = embed.weight
    if scale:
        masked_embed_weight = scale.expand_as(
            masked_embed_weight) * masked_embed_weight

    padding_idx = embed.padding_idx
    if padding_idx is None:
        padding_idx = -1

    X = embed._backend.Embedding.apply(words,
                                       masked_embed_weight,
                                       padding_idx,
                                       embed.max_norm,
                                       embed.norm_type,
                                       embed.scale_grad_by_freq,
                                       embed.sparse)
    return X