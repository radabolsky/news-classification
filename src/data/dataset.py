
from collections import Counter
import string

import datasets
from nltk.tokenize import word_tokenize
import torch
from tqdm.auto import tqdm


class WordDataset:
    def __init__(self, sentences, word2ind):
        self.data = sentences
        self.unk_id = word2ind['<unk>']
        self.bos_id = word2ind['<bos>']
        self.eos_id = word2ind['<eos>']
        self.pad_id = word2ind['<pad>']
        self.word2ind = word2ind

    def __getitem__(self, idx: int):
        processed_text = self.data[idx]['text'].lower().translate(
            str.maketrans('', '', string.punctuation))
        tokenized_sentence = [self.bos_id]
        tokenized_sentence += [
            self.word2ind.get(word, self.unk_id) for word in word_tokenize(processed_text)
            ] 
        tokenized_sentence += [self.eos_id]

        train_sample = {
            "text": tokenized_sentence,
            "label": self.data[idx]['label']
        }

        return train_sample

    def __len__(self) -> int:
        return len(self.data)
    

def get_word2ind(dataset: datasets.dataset_dict.DatasetDict, counter_thresh: int = 25):
    words = Counter()

    for example in tqdm(dataset['train']['text']):
        prccessed_text = example.lower().translate(
            str.maketrans('', '', string.punctuation))

        for word in word_tokenize(prccessed_text):
            words[word] += 1


    vocab = set(['<unk>', '<bos>', '<eos>', '<pad>'])
    counter_thresh = 25

    for char, cnt in words.items():
        if cnt > counter_thresh:
            vocab.add(char)

    print(f'Размер словаря: {len(vocab)}')

    word2ind = {char: i for i, char in enumerate(vocab)}
    ind2word = {i: char for char, i in word2ind.items()}
    return word2ind, ind2word, vocab


def collate_fn_with_padding(input_batch, pad_id, device, max_len=256):
    seq_lens = [len(x['text']) for x in input_batch]
    max_seq_len = min(max(seq_lens), max_len)

    new_batch = []
    for sequence in input_batch:
        sequence['text'] = sequence['text'][:max_seq_len]
        for _ in range(max_seq_len - len(sequence['text'])):
            sequence['text'].append(pad_id)

        new_batch.append(sequence['text'])

    sequences = torch.LongTensor(new_batch).to(device)
    labels = torch.LongTensor([x['label'] for x in input_batch]).to(device)

    new_batch = {
        'input_ids': sequences,
        'label': labels
    }

    return new_batch